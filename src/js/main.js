import '../css/main.sass';

$("document").ready(function () {
    function showResult(keyword) {
        var myUrl =
            "https://en.wikipedia.org/w/api.php?action=opensearch&format=json&search=" +
            keyword;
        $.ajax({
            url: myUrl,
            data: "queryData",
            dataType: "jsonp",
            contentType: "application/json; charset = utf-8",
            type: "GET",
            async: false,
            cache: false,
            success: function (result) {
                linkgenerator(result);
                console.log(result);
                console.log(keyword);
            }
        })
            .done(function () {
                console.log("success");
            })
            .fail(function () {
                console.log("error");
            })
            .always(function () {
                console.log("complete");
            });
    }

    function linkgenerator(result) {
        for (var i = 0; i < result[1].length; i++) {
            $("#content").append(
                "<div class='card content-box'><a href=" +
                result[3][i] +
                "><p class='content-box__title'>" +
                result[1][i] +
                "</p></a><p class='content-box__url truncate'>" +
                result[3][i] +
                "</p><p class='content-box__desc'>" +
                result[2][i] +
                "</p></div>"
            );
        }
    }

    $("#randomArticle").click(function () {
        window.open("https://en.wikipedia.org/wiki/special:random");
    });

    $(".close").click(function () {
        $("#content").empty();
        $("input").val("");
        $(".container").show();
        $("body").removeClass("blue lighten-3");
        $("body").append(
            "<footer class='footer center text-center flow-text z-depth-2'><p class='center center-text'>Written and coded by <a href='https://www.freecodecamp.com/piotrek1543' target='_blank''>Piotr Ekert</a>.</p></footer>"
        );
    });

    $("#search").focus(function () {
        $(".container").hide();
        $(".footer").remove();
        $("body").addClass("blue lighten-3");
        $("body").append(
            "<div class='fixed-action-btn'><a href='#' class='btn-floating red btn btn-large'><i class='large material-icons'>clear_all</i></a></div>"
        );

        $(".fixed-action-btn").click(function () {
            $("#content").empty();
            $("input").val("");
            $("body").removeClass("blue lighten-3");
            $(".container").show();
            $("body").append(
                "<footer class='footer center text-center flow-text z-depth-2'><p class='center center-text'>Written and coded by <a href='https://www.freecodecamp.com/piotrek1543' target='_blank''>Piotr Ekert</a>.</p></footer>"
            );

            $("body").removeClass("blue lighten-3");
            $(".fixed-action-btn").remove();
        });
    });

    $("#search").focusout(function () {
        if ($("#search").val() == "") {
            $("body").removeClass("blue lighten-3");
            $(".container").show();
            $("#content").empty();
            $(".fixed-action-btn").remove();
        }
    });

    $("#search").keyup(function () {
        var keyPressed = $(this).val();
        $("#content").empty();
        showResult(keyPressed);
    });
});
